package ca.csf.mobile1.tp1.chemical.compound;

/**
 * Created by Jeammy on 21/02/17.
 */
public interface ChemicalCompound {
    double getWeight();
}
