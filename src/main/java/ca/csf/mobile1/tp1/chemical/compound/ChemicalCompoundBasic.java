package ca.csf.mobile1.tp1.chemical.compound;

import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;

/**
 * Created by Jeammy on 21/02/17.
 */
public class ChemicalCompoundBasic implements ChemicalCompound{
    private ChemicalElement element;

    public ChemicalCompoundBasic(ChemicalElement element){
        /*if(element==null){
            throw new UnknownChemicalElementException(elementSymbol);
        }*/
        this.element = element;
    }

    public double getWeight(){
        return element.getWeight();
    }
}
