package ca.csf.mobile1.tp1.chemical.compound;

import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;
import ca.csf.mobile1.tp1.chemical.element.ChemicalElementRepository;

import java.util.ArrayList;



/**
 * Created by Jeammy on 21/02/17.
 */
public class ChemicalCompoundFactory {
    ChemicalElementRepository elements;

    public ChemicalCompoundFactory(ChemicalElementRepository elements){
        this.elements = elements;
    }

    /**Creer l'element chimique a partir d'une string.
     * @Param string: la string a decortiquer
     * @throws : les exception susceptibles de creer des erreurs
     * @return : l'element chimique
     */
    public ChemicalCompound createFromString(String string) throws Exception{

        checkExceptions(string);

        //--------------------------------------------------------------------------------------------------------------
        String elementSymbol = "";
        ArrayList<ChemicalCompound> list = new ArrayList<ChemicalCompound>();
        for (int i = 0; i < string.length(); i++) {

            //si le string contient un exposant
            if (string.charAt(i) == '(' && Character.isDigit(string.charAt(string.length()-1))) {
                int exp = Character.getNumericValue(string.charAt(string.length()-1));
                ChemicalCompoundExponent chem = new ChemicalCompoundExponent
                        (createFromString(string.substring(0,string.length()-1)),exp);
                list.add(chem);
                i=string.length()-1;
            }

            //si il y a un groupe.
            else if (string.charAt(i) == '(' && string.charAt(string.length()-1) == ')'){

                list.add(createFromString(string.substring(i+1,
                        string.length()-1)));
                i=string.length()-1;
            }

            //Decortique la string pour trouver un element.
            //Majuscule
            else if(Character.isUpperCase(string.charAt(i))){
                elementSymbol += string.charAt(i);
                if (i+1 < string.length()) {
                    if (!Character.isLowerCase(string.charAt(i+1))) {
                        if (Character.isDigit(string.charAt(i+1))) {
                            ChemicalCompoundExponent chem = new ChemicalCompoundExponent
                                    (createFromString(string.substring(i,i+1)),Character.getNumericValue(string.charAt
                                            (i+1)));
                            elementSymbol="";
                            list.add(chem);
                        }
                    }
                    else if(!Character.isLowerCase(string.charAt(i+1)) && !Character.isDigit(string.charAt(i+1))){
                        ChemicalElement elementToAdd = elements.get(Character.toString(string.charAt(i)));
                        if(elementToAdd==null){
                            throw new UnknownChemicalElementException(elementSymbol);
                        }

                        ChemicalCompoundBasic chem = new ChemicalCompoundBasic(elementToAdd);
                        elementSymbol="";
                        list.add(chem);
                    }
                }
                else{
                    ChemicalElement elementToAdd = elements.get(Character.toString(string.charAt(i)));
                    if(elementToAdd==null){
                        throw new UnknownChemicalElementException(elementSymbol);
                    }

                    ChemicalCompoundBasic chem = new ChemicalCompoundBasic(elementToAdd);
                    elementSymbol="";
                    list.add(chem);
                }
            }

            //Continent une minuscule
            else if (Character.isLowerCase(string.charAt(i))){
                elementSymbol += string.charAt(i);
                if (i+1 < string.length()) {
                    if(Character.isDigit(string.charAt(i+1))){
                        ChemicalCompoundExponent chem = new ChemicalCompoundExponent(createFromString(elementSymbol),
                                string.charAt(i+1));
                        elementSymbol="";
                        list.add(chem);
                    }
                    else{
                        ChemicalElement elementToAdd = elements.get(elementSymbol);
                        if(elementToAdd==null){
                            throw new UnknownChemicalElementException(elementSymbol);
                        }

                        ChemicalCompoundBasic chem = new ChemicalCompoundBasic(elementToAdd);
                        elementSymbol="";
                        list.add(chem);
                    }
                }
                else{
                    ChemicalElement elementToAdd = elements.get(elementSymbol);
                    if(elementToAdd==null){
                        throw new UnknownChemicalElementException(elementSymbol);
                    }

                    ChemicalCompoundBasic chem = new ChemicalCompoundBasic(elementToAdd);
                    elementSymbol="";
                    list.add(chem);
                }
            }
        }

        ChemicalCompoundGroup group = new ChemicalCompoundGroup(list);
        return group;
    }

    /**Verifie les exceptions possible de la formule donnee en parametre.
     * @Param string: la formule chimique.
     * @throws: les exception susceptibles de creer des erreurs
     */
    private void checkExceptions(String string)throws Exception{
        //EmptyFormulaException
        string = string.trim();
        string = string.split("\n")[0];
        string = string.split("\t")[0];

        if(string.isEmpty()){throw new EmptyFormulaException();}

        //IllegalClosingParenthesisException
        //MissingClosingParenthesisException
        int nbClosingParenthesis=0;
        int nbOpeningParenthesis=0;
        for (int i=0;i<string.length();i++){
            if(string.charAt(i)=='('){
                nbOpeningParenthesis++;
            }
            if(string.charAt(i)==')'){
                nbClosingParenthesis++;
            }
        }
        if (nbOpeningParenthesis<nbClosingParenthesis){throw new IllegalClosingParenthesisException();}
        if (nbOpeningParenthesis>nbClosingParenthesis){throw new MissingClosingParenthesisException();}

        //EmptyParenthesisException
        for(int i=0;i<string.length();i++){
            if (string.charAt(i)=='('&&string.charAt(i+1)==')'){throw new EmptyParenthesisException();}
        }

        //IllegalCharacterException
        for (int i = 0; i < string.length() ; i++) {
            if(!Character.isAlphabetic(string.charAt(i)) && !Character.isDigit(string.charAt(i))
                    && string.charAt(i) != '(' && string.charAt(i) != ')'){
                throw new IllegalCharacterException(string.charAt(i));
            }
        }

        //MisplacedExponentException
        for (int i=0;i<string.length();i++){
            if (Character.isDigit(string.charAt(i))){
                if(i==0) {
                    throw new MisplacedExponentException();
                }
                else if (string.charAt(i - 1) != ')' && !Character.isAlphabetic(string.charAt(i - 1))) {
                    throw new MisplacedExponentException();
                }

            }
        }
    }
}
