package ca.csf.mobile1.tp1.chemical.compound;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Jeammy on 21/02/17.
 */
public class ChemicalCompoundGroup implements ChemicalCompound {
    //private ChemicalCompound[] compounds;
    private ArrayList<ChemicalCompound> compounds;

    /*public ChemicalCompoundGroup(ChemicalCompound[] compounds){
        this.compounds = compounds;
    }*/
    public ChemicalCompoundGroup(ArrayList<ChemicalCompound> compounds){
        this.compounds = compounds;
    }
    public ChemicalCompoundGroup(ChemicalCompound...args){

        for (ChemicalCompound chem:args) {
            this.compounds.add(chem);
        }
    }

    public double getWeight(){
        double weight = 0;
        for(int i =0;i<compounds.size();i++){
            weight+=compounds.get(i).getWeight();
        }

        return weight;
    }
}
