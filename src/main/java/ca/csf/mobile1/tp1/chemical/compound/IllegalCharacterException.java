package ca.csf.mobile1.tp1.chemical.compound;

/**
 * Created by Jeammy on 21/02/17.
 */
public class IllegalCharacterException extends Exception {
    private char aChar;
    String message = "Illegal character \"%s\"found in chemical compound formula.";
    public IllegalCharacterException(char aChar){
        this.aChar = aChar;
    }
    public char getCharacter(){
        return aChar;
    }
    public String getMessage(){
        return String.format(message,aChar);
    }
}
