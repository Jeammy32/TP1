package ca.csf.mobile1.tp1.chemical.compound;

/**
 * Created by Jeammy on 21/02/17.
 */
public class IllegalClosingParenthesisException extends Exception{
    String message = "Chemical compound contains an illegal closing parenthesis before any opening parenthesis.";
    @Override
    public String getMessage(){
        return message;
    }
}
