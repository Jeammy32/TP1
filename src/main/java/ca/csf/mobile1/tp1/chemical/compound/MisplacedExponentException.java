package ca.csf.mobile1.tp1.chemical.compound;

/**
 * Created by Jeammy on 21/02/17.
 */
public class MisplacedExponentException extends Exception{
    String message ="Exponent found before any other chemical element or parenthesis.";
    @Override
    public String getMessage(){
        return message;
    }
}
