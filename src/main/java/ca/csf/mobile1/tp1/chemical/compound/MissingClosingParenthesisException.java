package ca.csf.mobile1.tp1.chemical.compound;

/**
 * Created by Jeammy on 21/02/17.
 */
public class MissingClosingParenthesisException extends Exception {
    String message = "Chemical compound contains is missing a closing parenthesis.";
    @Override
    public String getMessage(){
        return message;
    }
}
