package ca.csf.mobile1.tp1.chemical.compound;

import ca.csf.mobile1.tp1.chemical.element.ChemicalElement;

/**
 * Created by Jeammy on 21/02/17.
 */
public class UnknownChemicalElementException extends Exception {
    String elementName;
    String message = "Chemical element \"%s\" is unknown.";
    public UnknownChemicalElementException(String elementName){
        this.elementName=elementName;
    }

    public String getElement(){
        return elementName;
    }

    @Override
    public String getMessage(){
        return String.format(message,elementName);
    }
}
